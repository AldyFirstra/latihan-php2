<?php

require 'animal.php';
require 'ape.php';
require 'frog.php';


$sheep = new Animal("shaun");

echo "Name: ".$sheep->name; // "shaun"
echo "<br>Legs: ".$sheep->legs; // 4
echo "<br>Cold Blooded: ".$sheep->cold_blooded; // "no"

echo "<br> <br>";

$kodok = new frog("buduk");
echo "Name: ". $kodok->name;
echo "<br>Legs: ". $kodok->legs;
echo "<br>Cold Blooded: " . $kodok->cold_blooded . "<br>";
$kodok->get_jump() ; // "hop hop"

echo "<br> <br>";

$sungokong = new ape("kera sakti");
echo "Name: ". $sungokong->name;
echo "<br>Legs: ". $sungokong->legs;
echo "<br>Cold Blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->get_yell(); // "Auooo"



// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())